import { Component } from "react";
import { AtSearchBar, AtIcon } from "taro-ui";
import Taro from "@tarojs/taro";
import classnames from "classnames";
import { View, Text, Image, ScrollView } from "@tarojs/components";

import "./index.scss";

type PageState = {
  searchValue: string;
  //   hotList: Array<{
  //     searchWord: string,
  //     score: number,
  //     iconUrl: string,
  //     content: string,
  //     iconType: number
  //   }>,
  //   historyList: Array<string>
};

class Page extends Component<{}, PageState> {
  constructor(props) {
    super(props);
    this.state = {
      searchValue: "",
      //   hotList: [],
      //   historyList: []
    };
  }

  componentWillReceiveProps(nextProps) {
    console.log(this.props, nextProps);
  }

  componentDidMount() {
    // this.getHotSearch()
  }

  componentWillUnmount() {}

  componentDidShow() {
    // console.log('getKeywordInHistory()', getKeywordInHistory())
    // this.setState({
    //   historyList: getKeywordInHistory()
    // })
  }

  componentDidHide() {}

  //   searchTextChange(val) {
  //     this.setState({
  //       searchValue: val
  //     })
  //   }

  //   searchResult() {
  //     // this.goResult(this.state.searchValue)
  //   }

  //   goResult(keywords) {
  //     setKeywordInHistory(keywords)
  //     // Taro.navigateTo({
  //     //   url: `/pages/packageA/pages/searchResult/index?keywords=${keywords}`
  //     // })
  //   }

  //   clearKeywordInHistory() {
  //     this.setState({
  //       historyList: []
  //     })
  //     clearKeywordInHistory()
  //   }

  //   getHotSearch() {
  //     api.get('/search/hot/detail', {
  //     }).then((res) => {
  //       if (res.data && res.data.data) {
  //         this.setState({
  //           hotList: res.data.data
  //         })
  //       }
  //     })
  //   }
  onChange(value) {
    this.setState({
      searchValue: value,
    });
  }
  onActionClick() {
    console.log("开始搜索");
  }

  render() {
    return (
      <View className="search_container">
        <AtSearchBar
          actionName="搜一下"
          value={this.state.searchValue}
          onChange={this.onChange.bind(this)}
          onActionClick={this.onActionClick.bind(this)}
        />
      </View>
    );
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default Page;
