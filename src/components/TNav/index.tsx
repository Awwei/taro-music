// // import { FC, memo } from "react";
// // import { Image, ScrollView, View } from "@tarojs/components";
// // import "./index.scss";
// // import { AtIcon } from "taro-ui";

// // type Props = {
// //   IsImageShow: boolean;
// //   ImgSrc?: string;
// // };

// // const TNav: FC<Props> = ({ IsImageShow = false, ImgSrc = "" }) => {
// //   const AtIconDemo = <AtIcon value="TNav_Item_Icon" size="16" color="#ccc" />;
// //   const AtImageDemo = <Image src={ImgSrc} className="TNav_Item_img" />;

// //   return (
// //     // <ScrollView
// //     //         className="scrollview"
// //     //         scrollX
// //     //         scrollWithAnimation
// //     //         scrollTop={scrollTop}
// //     //         style={scrollStyle}
// //     //         lowerThreshold={Threshold}
// //     //         upperThreshold={Threshold}
// //     //         onScrollToUpper={this.onScrollToUpper.bind(this)} // 使用箭头函数的时候 可以这样写 `onScrollToUpper={this.onScrollToUpper}`
// //     //         onScroll={this.onScroll}
// //     //       >
// //     //         <View style={vStyleA}>A</View>
// //     //         <View style={vStyleB}>B</View>
// //     //         <View style={vStyleC}>C</View>
// //     //         <View style={vStyleD}>D</View>
// //     //         <View style={vStyleE}>E</View>
// //     //       </ScrollView>
// //     //   <View className="TNav_Item">
// //     //   {IsImageShow ? AtImageDemo : AtIconDemo}
// //     // </View>

// //   );
// // };

// // export default memo(TNav, (oldProps, newProps) => {
// //   return oldProps.IsImageShow === newProps.IsImageShow;
// // });
// import { ScrollView, View } from "@tarojs/components";
// import React, { Component } from "react";
// import { connect } from "react-redux";
// import { AtIcon } from "taro-ui";
// import TImage from "../TImage";
// //import * as actions from './storeModel/actionCreater'

// interface Props {
//   IsImageShow: boolean;
//   ImgSrc?: string;
// }
// interface State {
//   data: any;
// }
// class TNav extends Component<Props, State> {
//   constructor(props: Props) {
//     super(props);
//     this.state = {
//       data: [{
//         IsImageShow: false,

//       }],
//     };
//   }

//   scrollStyle = {
//     height: "150px",
//     width: "350px",
//     "white-space": "nowrap",
//   };
//   scrollTop = 0;
//   Threshold = 20;
//   vStyleA = {
//     display: "inline-block",
//     height: "150px",
//     width: "100px",
//     "background-color": "rgb(26, 173, 25)",
//   };
//   vStyleB = {
//     display: "inline-block",
//     height: "150px",
//     width: "100px",
//     "background-color": "rgb(39,130,215)",
//   };
//   vStyleC = {
//     display: "inline-block",
//     height: "150px",
//     width: "100px",
//     "background-color": "rgb(241,241,241)",
//     color: "#333",
//   };
//   vStyleD = {
//     display: "inline-block",
//     height: "150px",
//     width: "100px",
//     "background-color": "rgb(39,130,215)",
//     color: "#333",
//   };
//   vStyleE = {
//     display: "inline-block",
//     height: "150px",
//     width: "100px",
//     "background-color": "rgb(241,241,241)",
//     color: "#333",
//   };

//   onScrollToUpper() {}

//   // or 使用箭头函数
//   // onScrollToUpper = () => {}

//   onScroll(e) {
//     console.log(e.detail);
//   }
//   render() {
//     const { data } = this.state;
//     const { ImgSrc = "", IsImageShow = false } = this.props;

//     return (
//       <ScrollView
//         className="scrollview"
//         scrollX
//         scrollWithAnimation
//         scrollTop={this.scrollTop}
//         style={this.scrollStyle}
//         lowerThreshold={this.Threshold}
//         upperThreshold={this.Threshold}
//         onScrollToUpper={this.onScrollToUpper.bind(this)} // 使用箭头函数的时候 可以这样写 `onScrollToUpper={this.onScrollToUpper}`
//         onScroll={this.onScroll}
//       >
//         {data.map((item) => {
//           <View className="TNav_Item">
//             {IsImageShow ? <TImage ImgSrc={ImgSrc} /> : <AtIcon value="TNav_Item_Icon" size="16" color="#ccc" />}
//           </View>;
//         })}
//       </ScrollView>
//     );
//   }
// }

// const mapState = (state) => ({});

// const mapDispatch = (dispatch) => ({});

// export default connect(mapState, mapDispatch)(TNav);

import { FC } from "react";
import classnames from "classnames";
import { ScrollView, View } from "@tarojs/components";

import "./index.scss";
import TImage from "../TImage";
import { AtIcon } from "taro-ui";
type Props = {
  data: any;
};

const TNav: FC<Props> = ({ data }) => {
  const scrollStyle = {
    height: "150px",
    width: "350px",
    "white-space": "nowrap",
  };
  const scrollTop = 0;
  const Threshold = 20;
  const vStyleA = {
    display: "inline-block",
    height: "150px",
    width: "100px",
    "background-color": "rgb(26, 173, 25)",
  };

  function onScrollToUpper() {}

  // or 使用箭头函数
  // onScrollToUpper = () => {}

  function onScroll(e) {
    console.log(e.detail);
  }
  return (
    <ScrollView
      className="scrollview"
      scrollX
      scrollWithAnimation
      scrollTop={scrollTop}
      style={scrollStyle}
      lowerThreshold={Threshold}
      upperThreshold={Threshold}
      onScrollToUpper={onScrollToUpper.bind(this)} // 使用箭头函数的时候 可以这样写 `onScrollToUpper={this.onScrollToUpper}`
      onScroll={onScroll}
    >
      {data.map((item) => {
        return (
          <View className="TNav_Item">
            <View className="TNav_Item_Image">
              {item.TNavIsImageShow ? (
                <TImage ImgSrc={item.TNavImgSrc} />
              ) : (
                <AtIcon
                  prefixClass="fa"
                  value={item.TNavIconName}
                  size="16"
                  color="#a8e4e1"
                ></AtIcon>
              )}
            </View>
            <View className="TNav_Item_Text">{item.TNavName}</View>
          </View>
        );
      })}
      {/* {data.map(() => {
        return <View style={vStyleA}>文本</View>;
      })} */}
    </ScrollView>
  );
};

export default TNav;
