import  { FC, memo } from "react";
import {  Image } from "@tarojs/components";
import "./index.scss";

type Props = {
  ImgSrc: string;
};

const TImage: FC<Props> = ({ ImgSrc }) => {
  return <Image src={ImgSrc} className="Timage_img" />;
};

export default memo(TImage, (oldProps, newProps) => {
  return oldProps.ImgSrc === newProps.ImgSrc;
});
