

// 假设我们要使用 Redux
import { Provider } from "react-redux";
import configStore from "./store";
import "taro-ui/dist/style/index.scss"; // 全局引入一次即可
import "@/app.scss";
import "@/assets/iconFont/icon.scss";


const store = configStore();

function App(props) {
  return (
    // 在入口组件不会渲染任何内容，但我们可以在这里做类似于状态管理的事情
    <Provider store={store}>
      {/* props.children 是将要被渲染的页面 */}
      {props.children}
    </Provider>
  );

  // return props.children
}

export default App;
