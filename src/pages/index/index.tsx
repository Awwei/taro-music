// import React from "react";
import { View, Text, Swiper, SwiperItem, ScrollView } from "@tarojs/components";
import { useLoad } from "@tarojs/taro";
import "./index.scss";
import { AtButton, AtSwipeAction, AtTabBar } from "taro-ui";
import request from "@/services/requestOption";
import classnames from "classnames";
import Taro from "@tarojs/taro";

// export default function Index() {
//   useLoad(() => {
//     // getBanner();
//   });
//   let state = {
//     current: 0,
//     showLoading: true,
//     bannerList: [],
//     searchValue: "",
//   };
//   function getBanner() {
//     request
//       .get("/banner", {
//         type: 1,
//       })
//       .then(({ data }) => {
//         console.log("banner", data);
//         if (data.banners) {
//           // this.setState({
//           //   bannerList: data.banners,
//           // });
//         }
//       });
//   }

//   function switchTab(value: number) {
//     if (value !== 1) return;
//     Taro.reLaunch({
//       url: "/pages/my/index",
//     });
//   }
//   // const { recommendPlayList, song } = this.props;
//   // const { showLoading, bannerList, searchValue } = this.state;
//   // const { currentSongInfo, isPlaying, canPlayList } = song;
//   return (
//     // <View className={classnames({aaa:true})}>
//     //   <Text>c component</Text>
//     // </View>
//     <View
//       className={classnames({
//         index_container: true,
//       })}
//     >
//       <AtTabBar
//         fixed
//         selectedColor="#d43c33"
//         tabList={[
//           { title: "发现", iconPrefixClass: "fa", iconType: "feed" },
//           { title: "我的", iconPrefixClass: "fa", iconType: "music" },
//         ]}
//         onClick={switchTab.bind(this)}
//         current={state.current}
//       />
//     </View>
//   );
// }

import React, { Component } from "react";
import { connect } from "react-redux";
import TImage from "@/components/TImage";
import TNav from "@/components/TNav";
// import Loading from "@/components/Loading";
//import * as actions from './storeModel/actionCreater'

interface Props {}
interface State {
  showLoading: boolean;
  current: number;
  bannerList: Array<object>;
}
class Index extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      showLoading: true,
      current: 0,
      bannerList: [],
    };
  }
  componentWillMount(): void {
    this.getBanner();
  }

  getBanner() {
    // request
    //   .get("/banner", {
    //     type: 1,
    //   })
    //   .then(({ data }) => {
    //     console.log("banner", data);
    //     if (data.banners) {
    //       this.setState({
    //         bannerList: data.banners,
    //       });
    //     }
    //   });

    let data = {
      banners: [
        {
          pic: require("../../assets/images/test/IMG_9718.jpg"),
          targetId: 0,
          mainTitle: null,
          adid: null,
          targetType: 3000,
          titleColor: "blue",
          typeTitle: "独家策划",
          url: "https://y.music.163.com/m/at/653f714fb383dbffdb9ca049",
          adurlV2: null,
          exclusive: false,
          monitorImpress: null,
          monitorClick: null,
          monitorType: null,
          monitorImpressList: [],
          monitorClickList: [],
          monitorBlackList: null,
          extMonitor: null,
          extMonitorInfo: null,
          adSource: null,
          adLocation: null,
          encodeId: "0",
          program: null,
          event: null,
          video: null,
          dynamicVideoData: null,
          song: null,
          bannerId: "16986824409102361",
          alg: "banner-feature-1698682440910236",
          scm: "1.music-homepage.homepage_banner_force.banner.8342985.-1246915419.null",
          requestId: "",
          showAdTag: true,
          pid: null,
          showContext: null,
          adDispatchJson: null,
          s_ctrp: "syspf_resourceType_3000-syspf_resourceId_0",
          logContext: null,
          bannerBizType: "force_banner",
        },
        {
          pic: require("../../assets/images/test//IMG_9719.jpg"),
          targetId: 2090583673,
          mainTitle: null,
          adid: null,
          targetType: 1,
          titleColor: "red",
          typeTitle: "新歌首发",
          url: null,
          adurlV2: null,
          exclusive: false,
          monitorImpress: null,
          monitorClick: null,
          monitorType: null,
          monitorImpressList: [],
          monitorClickList: [],
          monitorBlackList: null,
          extMonitor: null,
          extMonitorInfo: null,
          adSource: null,
          adLocation: null,
          encodeId: "2090583673",
          program: null,
          event: null,
          video: null,
          dynamicVideoData: null,
          song: {
            name: "欠父亲的话",
            id: 2090583673,
            pst: 0,
            t: 0,
            ar: [
              {
                id: 6731,
                name: "赵雷",
                tns: [],
                alias: [],
              },
            ],
            alia: ["电影《我爸没说的那件事》主题曲"],
            pop: 100,
            st: 0,
            rt: "",
            fee: 8,
            v: 4,
            crbt: null,
            cf: "",
            al: {
              id: 176770118,
              name: "欠父亲的话",
              picUrl:
                "http://p2.music.126.net/TZbCJA1vEu4wY7xVU6Y7kA==/109951168980185514.jpg",
              tns: [],
              pic_str: "109951168980185514",
              pic: 109951168980185520,
            },
            dt: 322676,
            h: {
              br: 320000,
              fid: 0,
              size: 12909165,
              vd: -36592,
              sr: 48000,
            },
            m: {
              br: 192000,
              fid: 0,
              size: 7745517,
              vd: -33987,
              sr: 48000,
            },
            l: {
              br: 128000,
              fid: 0,
              size: 5163693,
              vd: -32285,
              sr: 48000,
            },
            sq: {
              br: 759770,
              fid: 0,
              size: 30645035,
              vd: -36245,
              sr: 48000,
            },
            hr: {
              br: 763709,
              fid: 0,
              size: 30803919,
              vd: -36377,
              sr: 48000,
            },
            a: null,
            cd: "01",
            no: 0,
            rtUrl: null,
            ftype: 0,
            rtUrls: [],
            djId: 0,
            copyright: 0,
            s_id: 0,
            mark: 537141248,
            originCoverType: 0,
            originSongSimpleData: null,
            tagPicList: null,
            resourceState: true,
            version: 4,
            songJumpInfo: null,
            entertainmentTags: null,
            single: 0,
            noCopyrightRcmd: null,
            rtype: 0,
            rurl: null,
            mst: 9,
            cp: 2713486,
            mv: 14669382,
            publishTime: 0,
            privilege: {
              id: 2090583673,
              fee: 8,
              payed: 0,
              st: 0,
              pl: 128000,
              dl: 0,
              sp: 7,
              cp: 1,
              subp: 1,
              cs: false,
              maxbr: 999000,
              fl: 128000,
              toast: false,
              flag: 4,
              preSell: false,
              playMaxbr: 999000,
              downloadMaxbr: 999000,
              maxBrLevel: "hires",
              playMaxBrLevel: "hires",
              downloadMaxBrLevel: "hires",
              plLevel: "standard",
              dlLevel: "none",
              flLevel: "standard",
              rscl: null,
              freeTrialPrivilege: {
                resConsumable: false,
                userConsumable: false,
                listenType: null,
              },
              chargeInfoList: [
                {
                  rate: 128000,
                  chargeUrl: null,
                  chargeMessage: null,
                  chargeType: 0,
                },
                {
                  rate: 192000,
                  chargeUrl: null,
                  chargeMessage: null,
                  chargeType: 1,
                },
                {
                  rate: 320000,
                  chargeUrl: null,
                  chargeMessage: null,
                  chargeType: 1,
                },
                {
                  rate: 999000,
                  chargeUrl: null,
                  chargeMessage: null,
                  chargeType: 1,
                },
                {
                  rate: 1999000,
                  chargeUrl: null,
                  chargeMessage: null,
                  chargeType: 1,
                },
              ],
            },
            alg: "banner-feature-1698754310153934",
          },
          bannerId: "16987543101539341",
          alg: "banner-feature-1698754310153934",
          scm: "1.music-homepage.homepage_banner_force.banner.8345978.-1246852050.null",
          requestId: "",
          showAdTag: true,
          pid: null,
          showContext: null,
          adDispatchJson: null,
          s_ctrp: "syspf_resourceType_1-syspf_resourceId_2090583673",
          logContext: null,
          bannerBizType: "force_banner",
        },
        {
          pic: require("../../assets/images/test/IMG_9718.jpg"),
          targetId: 0,
          mainTitle: null,
          adid: null,
          targetType: 3000,
          titleColor: "blue",
          typeTitle: "独家策划",
          url: "https://y.music.163.com/m/at/653f714fb383dbffdb9ca049",
          adurlV2: null,
          exclusive: false,
          monitorImpress: null,
          monitorClick: null,
          monitorType: null,
          monitorImpressList: [],
          monitorClickList: [],
          monitorBlackList: null,
          extMonitor: null,
          extMonitorInfo: null,
          adSource: null,
          adLocation: null,
          encodeId: "0",
          program: null,
          event: null,
          video: null,
          dynamicVideoData: null,
          song: null,
          bannerId: "1698682440910236",
          alg: "banner-feature-1698682440910236",
          scm: "1.music-homepage.homepage_banner_force.banner.8342985.-1246915419.null",
          requestId: "",
          showAdTag: true,
          pid: null,
          showContext: null,
          adDispatchJson: null,
          s_ctrp: "syspf_resourceType_3000-syspf_resourceId_0",
          logContext: null,
          bannerBizType: "force_banner",
        },
        {
          pic: require("../../assets/images/test//IMG_9719.jpg"),
          targetId: 2090583673,
          mainTitle: null,
          adid: null,
          targetType: 1,
          titleColor: "red",
          typeTitle: "新歌首发",
          url: null,
          adurlV2: null,
          exclusive: false,
          monitorImpress: null,
          monitorClick: null,
          monitorType: null,
          monitorImpressList: [],
          monitorClickList: [],
          monitorBlackList: null,
          extMonitor: null,
          extMonitorInfo: null,
          adSource: null,
          adLocation: null,
          encodeId: "2090583673",
          program: null,
          event: null,
          video: null,
          dynamicVideoData: null,
          song: {
            name: "欠父亲的话",
            id: 2090583673,
            pst: 0,
            t: 0,
            ar: [
              {
                id: 6731,
                name: "赵雷",
                tns: [],
                alias: [],
              },
            ],
            alia: ["电影《我爸没说的那件事》主题曲"],
            pop: 100,
            st: 0,
            rt: "",
            fee: 8,
            v: 4,
            crbt: null,
            cf: "",
            al: {
              id: 176770118,
              name: "欠父亲的话",
              picUrl:
                "http://p2.music.126.net/TZbCJA1vEu4wY7xVU6Y7kA==/109951168980185514.jpg",
              tns: [],
              pic_str: "109951168980185514",
              pic: 109951168980185520,
            },
            dt: 322676,
            h: {
              br: 320000,
              fid: 0,
              size: 12909165,
              vd: -36592,
              sr: 48000,
            },
            m: {
              br: 192000,
              fid: 0,
              size: 7745517,
              vd: -33987,
              sr: 48000,
            },
            l: {
              br: 128000,
              fid: 0,
              size: 5163693,
              vd: -32285,
              sr: 48000,
            },
            sq: {
              br: 759770,
              fid: 0,
              size: 30645035,
              vd: -36245,
              sr: 48000,
            },
            hr: {
              br: 763709,
              fid: 0,
              size: 30803919,
              vd: -36377,
              sr: 48000,
            },
            a: null,
            cd: "01",
            no: 0,
            rtUrl: null,
            ftype: 0,
            rtUrls: [],
            djId: 0,
            copyright: 0,
            s_id: 0,
            mark: 537141248,
            originCoverType: 0,
            originSongSimpleData: null,
            tagPicList: null,
            resourceState: true,
            version: 4,
            songJumpInfo: null,
            entertainmentTags: null,
            single: 0,
            noCopyrightRcmd: null,
            rtype: 0,
            rurl: null,
            mst: 9,
            cp: 2713486,
            mv: 14669382,
            publishTime: 0,
            privilege: {
              id: 2090583673,
              fee: 8,
              payed: 0,
              st: 0,
              pl: 128000,
              dl: 0,
              sp: 7,
              cp: 1,
              subp: 1,
              cs: false,
              maxbr: 999000,
              fl: 128000,
              toast: false,
              flag: 4,
              preSell: false,
              playMaxbr: 999000,
              downloadMaxbr: 999000,
              maxBrLevel: "hires",
              playMaxBrLevel: "hires",
              downloadMaxBrLevel: "hires",
              plLevel: "standard",
              dlLevel: "none",
              flLevel: "standard",
              rscl: null,
              freeTrialPrivilege: {
                resConsumable: false,
                userConsumable: false,
                listenType: null,
              },
              chargeInfoList: [
                {
                  rate: 128000,
                  chargeUrl: null,
                  chargeMessage: null,
                  chargeType: 0,
                },
                {
                  rate: 192000,
                  chargeUrl: null,
                  chargeMessage: null,
                  chargeType: 1,
                },
                {
                  rate: 320000,
                  chargeUrl: null,
                  chargeMessage: null,
                  chargeType: 1,
                },
                {
                  rate: 999000,
                  chargeUrl: null,
                  chargeMessage: null,
                  chargeType: 1,
                },
                {
                  rate: 1999000,
                  chargeUrl: null,
                  chargeMessage: null,
                  chargeType: 1,
                },
              ],
            },
            alg: "banner-feature-1698754310153934",
          },
          bannerId: "1698754310153934",
          alg: "banner-feature-1698754310153934",
          scm: "1.music-homepage.homepage_banner_force.banner.8345978.-1246852050.null",
          requestId: "",
          showAdTag: true,
          pid: null,
          showContext: null,
          adDispatchJson: null,
          s_ctrp: "syspf_resourceType_1-syspf_resourceId_2090583673",
          logContext: null,
          bannerBizType: "force_banner",
        },
      ],
    };

    this.setState({
      bannerList: data.banners,
    });
  }

  handleTabBarClick(value) {
    this.setState({
      current: value,
    });
  }

  render() {
    // const { showLoading } = this.state;
    const { bannerList } = this.state;

    // nav栏的模拟数据
    const data = [
      {
        TNavIsImageShow: false,
        TNavImgSrc: "",
        TNavIconName: "calendar-minus-o",
        TNavName: "每日推荐",
      },
      {
        TNavIsImageShow: true,
        TNavImgSrc: require("../../assets/images/temp/a-tupianyihuifu-10.png"),
        TNavIconName: "",
        TNavName: "私人漫游",
      },
      {
        TNavIsImageShow: true,
        TNavImgSrc: require("../../assets/images/temp/gedan.png"),
        TNavIconName: "",
        TNavName: "歌单",
      },
      {
        TNavIsImageShow: false,
        TNavImgSrc: "",
        TNavIconName: "bar-chart",
        TNavName: "排行榜",
      },
      {
        TNavIsImageShow: true,
        TNavImgSrc: require("../../assets/images/temp/zhizuoguocheng.png"),
        TNavIconName: "",
        TNavName: "数字专辑",
      },

      {
        TNavIsImageShow: true,
        TNavImgSrc: require("../../assets/images/temp/newsong.png"),
        TNavIconName: "",
        TNavName: "新歌",
      },
    ];

    return (
      <View className={classnames({ index_container: true })}>
        {/* <Loading fullPage={true} hide={!showLoading} /> */}
        {/* 头部栏 */}
        <View className="index_header">
          <View className="index_header__logo">
            <TImage
              ImgSrc={require("../../assets/images/temp/cm2_icn_list2@2x.png")}
            />
          </View>
          <View className="index_header__Input">
            <View className="index_header__Input__Seach">
              <TImage
                ImgSrc={require("../../assets/images/temp/sousuo_o.png")}
              />
            </View>
            <Text className="index_header__Input__Text">裂缝中的阳光 - </Text>
          </View>
        </View>
        {/* 轮播栏 */}
        <View className="index_Swiper">
          <Swiper
            className="banner_list"
            indicatorColor="#999"
            indicatorActiveColor="#ffffff"
            circular
            indicatorDots
            autoplay
          >
            {bannerList.map((item: any) => (
              <SwiperItem key={item.bannerId} className="banner_list__item">
                <View className="banner_list__item__img">
                  <TImage ImgSrc={item.pic} />
                </View>
              </SwiperItem>
            ))}
          </Swiper>
        </View>
        {/* Nav导航栏 */}
        <View className="index_Nav">
          <TNav data={data} />
        </View>
        
        {/* 底部导航栏 */}
        <AtTabBar
          fixed
          backgroundColor="#b6dfe7"
          selectedColor="#173e3a"
          iconSize={20}
          fontSize={12}
          tabList={[
            {
              title: "发现",
              image: require("../../assets/images/temp/cm2_btm_icn_discovery.png"),
              selectedImage: require("../../assets/images/temp/netease-cloud-music-line.png"),
            },
            {
              title: "我的",
              image: require("../../assets/images/temp/cm2_btm_icn_music.png"),
              selectedImage: require("../../assets/images/temp/cm2_btm_icn_music_prs2.png"),
            },
          ]}
          onClick={this.handleTabBarClick.bind(this)}
          current={this.state.current}
        />
      </View>
    );
  }
}

const mapState = (state) => ({});

const mapDispatch = (dispatch) => ({});

export default connect(mapState, mapDispatch)(Index);
